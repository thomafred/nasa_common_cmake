Change log
==========

2.2.3
-----

- Added CHANGELOG.md
- Added LICENSE.md
- Removed trusty builds from CI
