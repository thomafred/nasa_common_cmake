# - Find Log4cpp
# Find the ROS or native Log4cpp includes and library
#
#  Log4cpp_FOUND       - True if Log4cpp found.
#  Log4cpp_INCLUDE_DIR - where to find log4cpp/Category.hh, etc.
#  Log4cpp_LIBRARIES   - List of libraries when using Log4cpp.

find_package(PkgConfig)
pkg_search_module(PC_LOG4CPP orocos-log4cpp log4cpp)
# message(STATUS "PKG_CONFIG_FOUND: ${PKG_CONFIG_FOUND}")
# message(STATUS "PKG_CONFIG_EXECUTABLE: ${PKG_CONFIG_EXECUTABLE}")
# message(STATUS "PKG_CONFIG_VERSION_STRING: ${PKG_CONFIG_VERSION_STRING}")
# message(STATUS "PC_LOG4CPP_FOUND: ${PC_LOG4CPP_FOUND}")
# message(STATUS "PC_LOG4CPP_INCLUDE_DIRS: ${PC_LOG4CPP_INCLUDE_DIRS}")
# message(STATUS "PC_LOG4CPP_LIBRARY_DIRS: ${PC_LOG4CPP_LIBRARY_DIRS}")

find_path(LOG4CPP_INCLUDE_DIR log4cpp/Category.hh
          HINTS ${PC_LOG4CPP_INCLUDE_DIRS}
          PATH_SUFFIXES include)

find_library(LOG4CPP_LIBRARY NAMES orocos-log4cpp log4cpp
             HINTS ${PC_LOG4CPP_LIBRARY_DIRS})

set(Log4cpp_LIBRARIES ${LOG4CPP_LIBRARY})
set(Log4cpp_INCLUDE_DIRS ${LOG4CPP_INCLUDE_DIR})
# message(STATUS "LOG4CPP_LIBRARY: ${LOG4CPP_LIBRARY}")
# message(STATUS "LOG4CPP_INCLUDE_DIR: ${LOG4CPP_INCLUDE_DIR}")

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LOG4CPP_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(Log4cpp DEFAULT_MSG
                                  Log4cpp_LIBRARIES Log4cpp_INCLUDE_DIRS)

mark_as_advanced(Log4cpp_INCLUDE_DIRS Log4cpp_LIBRARIES)

# ensure that they are cached
SET(Log4cpp_INCLUDE_DIRS ${Log4cpp_INCLUDE_DIRS} CACHE INTERNAL "The Log4cpp include path")
SET(Log4cpp_LIBRARIES ${Log4cpp_LIBRARIES} CACHE INTERNAL "The libraries needed to use Log4cpp library")
