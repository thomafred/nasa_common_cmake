# nasa_common_cmake

## About

See [package.xml](./package.xml) for version and maintainer information.

See [LICENSE.md](./LICENSE.md) for complete license information.  

This package contains cmake `FindModule.cmake` files as well as some cmake macros. The `FindModule.cmake` files contained include,

- FindCrypto.cmake
- FindFlyCapture.cmake
- FindGoogleMock.cmake
- FindJsonCpp.cmake
- FindLZO.cmake
- FindLog4cpp.cmake
- FindMesaSR.cmake
- FindProtocolBuffers.cmake
- FindYamlCpp.cmake
- FindZeroMQ.cmake

The file `InstallSymlink.cmake` contains a cmake macro for creating symlinks of cmake targets. 

#### Credits
- NASA ER4
